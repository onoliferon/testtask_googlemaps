//
//  LocationPoint.m
//  MapTestTask
//
//  Created by Olifer Nikita on 03.12.14.
//  Copyright (c) 2014 fer-comp. All rights reserved.
//

#import "LocationPoint.h"

@implementation LocationPoint

- (instancetype)initWithLatitude:(double)latitude longitude:(double)longitude {
    self = [super init];
    if (self) {
        _latitude = latitude;
        _longitude = longitude;
    }
    return self;
}

- (void)fetchWithJsonDict:(NSDictionary *)jsonDict error:(NSError **)error {
    
    _latitude = [jsonDict[@"latitude"][@"value"] doubleValue];
    _longitude = [jsonDict[@"longitude"][@"value"] doubleValue];
}

@end
