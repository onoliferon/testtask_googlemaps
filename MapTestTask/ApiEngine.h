//
//  ApiEngine.h
//  MapTestTask
//
//  Created by Olifer Nikita on 16.12.14.
//  Copyright (c) 2014 fer-comp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocationPoint.h"
#import "LocationsRoute.h"

typedef void(^RequestResultBlock)(id, NSError *);

// Singletone for retrieving data from internet
@interface ApiEngine : NSObject

+ (ApiEngine *)sharedInstance;

- (void)requestRouteForStart:(LocationPoint *)start end:(LocationPoint *)end finish:(RequestResultBlock)finishBlock;

- (void)requestSourceLocationPointWithFinishBlock:(RequestResultBlock)finishBlock;

- (void)requestDestinationLocationPointWithFinishBlock:(RequestResultBlock)finishBlock;

@end
