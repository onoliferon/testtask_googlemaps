//
//  JSONManuallyDeserializable.h
//  MapTestTask
//
//  Created by Olifer Nikita on 17.12.14.
//  Copyright (c) 2014 fer-comp. All rights reserved.
//

#import <Foundation/Foundation.h>

// Protocol for class that cannot be parsed by Mantle and must be parsed manually
@protocol JSONManuallyDeserializable <NSObject>

// Fetches object properties from json dictionary
- (void)fetchWithJsonDict:(NSDictionary *)jsonDict error:(NSError **)error;

@end
