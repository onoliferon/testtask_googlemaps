//
//  LocationPoint.h
//  MapTestTask
//
//  Created by Olifer Nikita on 03.12.14.
//  Copyright (c) 2014 fer-comp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle.h>
#import "JSONManuallyDeserializable.h"

// Point on map
@interface LocationPoint : MTLModel<JSONManuallyDeserializable>

@property (nonatomic) double latitude;
@property (nonatomic) double longitude;

-(instancetype)initWithLatitude:(double)latitude longitude:(double)longitude;

@end
