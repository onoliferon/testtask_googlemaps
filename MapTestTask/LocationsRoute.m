//
//  LocationsRoute.m
//  MapTestTask
//
//  Created by Olifer Nikita on 03.12.14.
//  Copyright (c) 2014 fer-comp. All rights reserved.
//

#import "LocationsRoute.h"
#import "ErrorsHelper.h"

@implementation LocationsRoute

- (void)fetchWithJsonDict:(NSDictionary *)jsonDict error:(NSError *__autoreleasing *)error {
    
    NSString *status = jsonDict[@"status"];
    if (![status isEqualToString:@"OK"]) {
        *error = [ErrorsHelper createErrorWithCode:ERROR_CODE_LOCATION message:status];
        return;
    }
    
    NSArray *routesArr = jsonDict[@"routes"];
    NSDictionary *routeDict = routesArr[0];
    NSArray *legsArr = routeDict[@"legs"];
    NSDictionary *legDict = legsArr[0];
    
    _distanceText = legDict[@"distance"][@"text"];
    _durationText = legDict[@"duration"][@"text"];
    
    _sourceAddress = legDict[@"start_address"];
    _destinationAddress = legDict[@"end_address"];
    
    _encodedRoutePolyline = routeDict[@"overview_polyline"][@"points"];
}

@end
