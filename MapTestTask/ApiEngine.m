//
//  ApiEngine.m
//  MapTestTask
//
//  Created by Olifer Nikita on 16.12.14.
//  Copyright (c) 2014 fer-comp. All rights reserved.
//

#import "ApiEngine.h"
#import <AFNetworking.h>
#import "ErrorsHelper.h"
#import "LocationsRoute.h"

@implementation ApiEngine

#pragma mark Singleton

+ (instancetype)sharedInstance {
    
    static ApiEngine *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [ApiEngine new];
    });
    return sharedInstance;
}

#pragma mark Requests

- (void)requestSourceLocationPointWithFinishBlock:(RequestResultBlock)finishBlock {
    [self requestPointByName:@"sample_pos1.json" finishBlock:finishBlock];
}

- (void)requestDestinationLocationPointWithFinishBlock:(RequestResultBlock)finishBlock {
    [self requestPointByName:@"sample_pos2.json" finishBlock:finishBlock];
}

- (void)requestRouteForStart:(LocationPoint *)start end:(LocationPoint *)end finish:(RequestResultBlock)finishBlock{
    
    static NSString * const url = @"http://maps.googleapis.com/maps/api/directions/json";
    
    NSDictionary *params = @{@"origin" : [NSString stringWithFormat:@"%lf,%lf", start.latitude, start.longitude],
                             @"destination" : [NSString stringWithFormat:@"%lf,%lf", end.latitude, end.longitude],
                             @"sensor" : @"false" };
    
    [self executeGetRequest:url withParams:params modelClass:[LocationsRoute class] finishBLock:finishBlock];
}

#pragma mark Private requests

- (void)requestPointByName:(NSString *)name finishBlock:(RequestResultBlock)finishBlock {
    
    NSString *url = [NSString stringWithFormat:@"https://dl.dropboxusercontent.com/u/200455/work/rtest-pos/%@", name];
    [self executeGetRequest:url modelClass:[LocationPoint class] finishBLock:finishBlock];
}


#pragma mark Requests executing

- (void)executeGetRequest:(NSString *)url modelClass:(Class)modelClass finishBLock:(RequestResultBlock)finishBlock {
    
    [self executeGetRequest:url withParams:nil modelClass:modelClass finishBLock:finishBlock];
}

- (void)executeGetRequest:(NSString *)url withParams:(NSDictionary *)params modelClass:(Class)modelClass finishBLock:(RequestResultBlock)finishBlock {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"application/json", nil];
    
    [manager GET:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        NSError *error = nil;
        id resultModel = nil;
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            
            if ([modelClass conformsToProtocol:@protocol(JSONManuallyDeserializable)]) {
                // Parse model manually
                
                resultModel = [modelClass new];
                [resultModel fetchWithJsonDict:responseObject error:&error];
            } else {
                // Parse by Mantle
                
                resultModel = [MTLJSONAdapter modelOfClass:modelClass fromJSONDictionary:responseObject error:&error];
            }
        } else {
            error = [ErrorsHelper createErrorWithCode:ERROR_CODE_JSON message:@"Not a dictionary returned"];
        }
        
        finishBlock(resultModel, error);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        finishBlock(nil, error);
    }];
}

@end
