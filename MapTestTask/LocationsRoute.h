//
//  LocationsRoute.h
//  MapTestTask
//
//  Created by Olifer Nikita on 03.12.14.
//  Copyright (c) 2014 fer-comp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle.h>
#import "LocationPoint.h"
#import "JSONManuallyDeserializable.h"

// Route between two location points, received from Google Direction Api
@interface LocationsRoute : MTLModel<JSONManuallyDeserializable>

@property (nonatomic) NSString *sourceAddress;
@property (nonatomic) NSString *destinationAddress;

@property (nonatomic) NSString *encodedRoutePolyline;
@property (nonatomic) NSString *distanceText;
@property (nonatomic) NSString *durationText;

@end
