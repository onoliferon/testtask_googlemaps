//
//  AbstractViewController.m
//  MapTestTask
//
//  Created by Olifer Nikita on 17.12.14.
//  Copyright (c) 2014 fer-comp. All rights reserved.
//

#import "AbstractViewController.h"

@implementation AbstractViewController

// Loads view from xib with the same name as that object concrete class
- (void)loadView {
    [super loadView];
    self.view = [[[NSBundle mainBundle] loadNibNamed:[[self class] description] owner:self options:nil] lastObject];
}


@end
