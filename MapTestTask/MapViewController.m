//
//  ViewController.m
//  MapTestTask
//
//  Created by Olifer Nikita on 02.12.14.
//  Copyright (c) 2014 fer-comp. All rights reserved.
//

#import "MapViewController.h"
#import "ApiEngine.h"
#import <GoogleMaps/GoogleMaps.h>

@interface MapViewController ()

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UILabel *routeInfoLabel;

@property (weak, nonatomic) GMSMarker *markerSource;
@property (weak, nonatomic) GMSMarker *markerDestination;

@end

@implementation MapViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self stylizeInfoView];
    _infoView.hidden = YES;
    
    // Cascade requesting source and destination points
    [[ApiEngine sharedInstance] requestSourceLocationPointWithFinishBlock:^(LocationPoint *sourcePoint, NSError *errorSource) {
        [[ApiEngine sharedInstance] requestDestinationLocationPointWithFinishBlock:^(LocationPoint *destinationPoint, NSError *errorDestination) {
            
            if (errorSource) {
                [self showError:errorSource];
                return;
            }
            if (errorDestination) {
                [self showError:errorDestination];
                return;
            }
            
            _markerSource = [self placePoint:sourcePoint];
            _markerDestination = [self placePoint:destinationPoint];
            
            _markerSource.title = NSLocalizedString(@"start point title", @"");
            _markerDestination.title = NSLocalizedString(@"end point title", @"");
            
            [self focusMapToShowMarkers:@[_markerSource, _markerDestination]];
            
            // Request route between points
            [[ApiEngine sharedInstance] requestRouteForStart:sourcePoint end:destinationPoint finish:^(LocationsRoute *route, NSError *error) {
                
                if (error) {
                    [self showError:error];
                    return;
                }
                
                GMSPolyline *polyline = [GMSPolyline polylineWithPath:[GMSPath pathFromEncodedPath:route.encodedRoutePolyline]];
                polyline.map = _mapView;
                
                _markerSource.snippet = route.sourceAddress;
                _markerDestination.snippet = route.destinationAddress;
                
                _infoView.hidden = NO;
                _routeInfoLabel.text = [NSString stringWithFormat:@"%@, %@", route.distanceText, route.durationText];
            }];
            
        }];
    }];
    
    _mapView.myLocationEnabled = YES;
}

- (GMSMarker *)placePoint:(LocationPoint *)point {
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(point.latitude, point.longitude);
    marker.map = _mapView;
    return marker;
}

- (void)focusMapToShowMarkers:(NSArray *)markers {
    
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    
    for (GMSMarker *marker in markers) {
        bounds = [bounds includingCoordinate:marker.position];
    }
    [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:30.0f]];
}

- (void)showError:(NSError *)error {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error message title", @"")
                                                    message:error.localizedDescription
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"error message btn ok", @"")
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)stylizeInfoView
{
    CALayer *layer = _infoView.layer;
    
    [layer setMasksToBounds:NO];
    [layer setCornerRadius:2];
    
    [layer setShadowColor:[UIColor blackColor].CGColor];
    [layer setShadowOffset:CGSizeMake(2, 2)];
    [layer setShadowOpacity:0.3];
    [layer setShadowRadius:1];
}

@end
