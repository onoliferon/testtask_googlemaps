//
//  Errors.m
//  MapTestTask
//
//  Created by Olifer Nikita on 03.12.14.
//  Copyright (c) 2014 fer-comp. All rights reserved.
//

#import "ErrorsHelper.h"

#define ERROR_DOMAIN @"ru.fer.MapTestTask.Error"

@implementation ErrorsHelper

+ (NSError *)createErrorWithCode:(int)code message:(NSString *)message {
    
    NSDictionary *userInfo = @{ NSLocalizedDescriptionKey : message };
    return [NSError errorWithDomain:ERROR_DOMAIN code:code userInfo:userInfo];
}

@end
