//
//  AbstractViewController.h
//  MapTestTask
//
//  Created by Olifer Nikita on 17.12.14.
//  Copyright (c) 2014 fer-comp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AbstractViewController : UIViewController

@end
