//
//  Errors.h
//  MapTestTask
//
//  Created by Olifer Nikita on 03.12.14.
//  Copyright (c) 2014 fer-comp. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ERROR_CODE_LOCATION 1
#define ERROR_CODE_JSON 2

// Helper for creating NSErrors in project's domain
@interface ErrorsHelper : NSObject

+ (NSError *)createErrorWithCode:(int)code message:(NSString *)message;

@end
